HomeFinder
============

It's a web app to find the best area to buy or rent a home.
Given three of user's most frequently visited places, it finds an area close to them all and also lists interesting places to visit in the vicinity (the latter is not finished yet.)

To run it just open `index.html` in a browser of your choice.

If it doesn't work as expected, you may need to manually type this in the browser console:
http://paste2.org/4M6d8Hc9